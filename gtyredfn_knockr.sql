-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 11, 2019 at 09:29 AM
-- Server version: 5.5.61-38.13-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gtyredfn_knockr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_users`
--

CREATE TABLE `tbl_admin_users` (
  `admin_users_id` int(11) NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `d_password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_studio_admin` int(11) NOT NULL,
  `is_vendor_admin` int(11) NOT NULL,
  `is_super_admin` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `expire_on` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admin_users`
--

INSERT INTO `tbl_admin_users` (`admin_users_id`, `username`, `password`, `d_password`, `is_studio_admin`, `is_vendor_admin`, `is_super_admin`, `status`, `expire_on`, `created_by`, `created_date`) VALUES
(3, 'admin', '0192023a7bbd73250516f069df18b500', 'admin123', 0, 0, 1, 0, '0000-00-00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `is_delete` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`country_id`, `country_name`, `is_delete`, `created_by`, `created_date`) VALUES
(1, 'India', 1, 3, '2019-03-04 15:45:09'),
(2, 'China', 1, 3, '2019-03-04 15:50:31'),
(3, 'USA', 0, 3, '2019-03-06 12:56:06'),
(4, 'China', 1, 3, '2019-03-06 13:00:15'),
(5, 'India', 1, 3, '2019-03-06 13:15:49'),
(6, 'India', 1, 3, '2019-03-06 13:27:49'),
(7, 'India', 0, 3, '2019-03-06 13:45:39'),
(8, 'dsd', 1, 3, '2019-03-07 13:29:54'),
(9, 'Australia', 1, 3, '2019-03-21 17:43:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room`
--

CREATE TABLE `tbl_room` (
  `room_id` int(11) NOT NULL,
  `room_name` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `latitude` varchar(150) NOT NULL,
  `longitude` varchar(150) NOT NULL,
  `radius` varchar(30) NOT NULL,
  `center_latitude` varchar(50) NOT NULL,
  `center_longitude` varchar(50) NOT NULL,
  `width` varchar(15) NOT NULL,
  `breadth` varchar(15) NOT NULL,
  `country_id` varchar(15) NOT NULL,
  `state_id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `is_delete` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room`
--

INSERT INTO `tbl_room` (`room_id`, `room_name`, `address`, `latitude`, `longitude`, `radius`, `center_latitude`, `center_longitude`, `width`, `breadth`, `country_id`, `state_id`, `description`, `is_delete`, `created_by`, `created_date`) VALUES
(1, 'New Room 2023', 'Thorat Mala Rd, West Supane, Maharashtra 415114, India', '17.307828702842183', '74.09844221875005', '', '', '', '2562', '21544', '1', 1, '', 1, 3, '2019-03-04 17:30:44'),
(2, 'Abcd7', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '90', '78', '1', 1, '', 1, 3, '2019-03-05 17:05:16'),
(3, 'H8', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '9', '00', '1', 1, '', 1, 3, '2019-03-05 17:09:28'),
(4, 'A', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '9', '67', '1', 1, '', 1, 3, '2019-03-05 17:53:41'),
(5, 'B', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '90', '00', '1', 3, '', 1, 3, '2019-03-05 17:53:53'),
(6, 'C', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '90', '00', '1', 2, '', 1, 3, '2019-03-05 17:54:04'),
(7, 'D', 'Cannot determine address at this location.', '20.03444423668543', '71.17607893750005', '', '', '', '9', '00', '1', 3, '', 1, 3, '2019-03-05 17:54:13'),
(8, 'Abcd7', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '90', '00', '1', 3, '', 1, 3, '2019-03-05 17:54:23'),
(9, 'Abcdr', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '90', '00', '1', 1, '', 1, 3, '2019-03-05 17:54:42'),
(10, 'B', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '90', '00', '1', 1, '', 1, 3, '2019-03-05 17:54:51'),
(11, 'E', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '90', '00', '1', 3, '', 1, 3, '2019-03-05 17:56:23'),
(12, 'B', '204, Mote Mangal Karyalay Rd, Bhavani Peth, Shobhapur, Kasba Peth, Pune, Maharashtra 411011, India', '18.52043', '73.85674300000005', '', '', '', '9', '00', '1', 1, '', 1, 3, '2019-03-05 18:00:05'),
(13, 'New Mexico Room', 'Unnamed Road, Corona, NM 88318, USA', '34.08831601591165', '-105.57196793749995', '', '', '', '100', '200', '3', 4, '', 1, 3, '2019-03-06 13:17:37'),
(14, 'Abcd', '28 Miner St, Boston, MA 02215, USA', '42.345928', '-71.102509', '', '', '', '90', '90', '3', 13, '', 1, 3, '2019-03-06 14:55:14'),
(15, 'A', 'Khaikheda, Madhya Pradesh 466661, India', '23.467742428942397', '77.19094803124995', '', '', '', '90', '00', '7', 14, '', 1, 3, '2019-03-06 15:37:19'),
(16, 'Abcd', 'Usak, U?ak Merkez/U?ak, Turkey', '38.6742286', '29.4058825', '', '', '', '90', '90', '3', 4, '', 1, 3, '2019-03-06 17:24:40'),
(17, 'A', 'Ghat Palasi, Madhya Pradesh 466651, India', '23.447585714871376', '77.05911209374995', '', '', '', '90', '00', '7', 22, '', 1, 3, '2019-03-06 17:54:15'),
(18, 'Asdasd', '9323 Hardy Dr, Overland Park, KS 66212, USA', '38.95940898442121', '-94.68017615625001', '', '', '', '65465', '65465465', '3', 4, '', 1, 3, '2019-03-06 18:13:17'),
(19, 'Test Room', 'Unnamed Road, Shendi, Maharashtra 414601, India', '19.1784862', '74.751263', '', '', '', '775588', '552255', '3', 4, '', 1, 3, '2019-03-06 18:19:22'),
(20, 'Sdfsfd', 'sdfsdfsdfsdf? Iran', '32.427908', '53.688046', '', '', '', '666', '5454', '3', 4, '', 1, 3, '2019-03-06 18:43:12'),
(21, 'A', '234, Khabra Rd, Gannipur, Muzaffarpur, Bihar 842001, India', '26.1118462', '85.3832407', '', '', '', '22', '33', '3', 4, '', 1, 3, '2019-03-07 15:59:09'),
(22, 'Washinton Square Park', 'Road 2N, Mather, MB R0K 1L0, Canada', '49.029532', '-99.1778756', '659', '49.029532', '-99.1778756', '50', '50', '3', 4, '', 1, 3, '2019-03-07 20:20:17'),
(23, 'Whole Foods Market, Upper West', '5065 Cessna Dr, Riverton, WY 82501, USA', '43.0570795', '-108.4600377', '2635', '43.0570795', '-108.4600377', '50', '50', '3', 4, '', 1, 3, '2019-03-07 22:09:48'),
(24, 'WTC', 'Orchard, CO 80649, USA', '40.3310766', '-104.1180962', '2565', '40.3310766', '-104.1180962', '50', '50', '3', 4, '', 1, 3, '2019-03-07 23:02:45'),
(25, 'Test Room', 'Co Rd 202, Stanford, MT 59479, USA', '47.1561577', '-110.215695', '11111', '47.1561577', '-110.215695', '', '', '3', 28, '', 1, 3, '2019-03-08 14:58:50'),
(26, 'Test', '65748 703 Rd, Falls City, NE 68355, USA', '40.0161461', '-95.4939052', '2256644', '40.0161461', '-95.4939052', '', '', '3', 33, '', 1, 3, '2019-03-08 15:23:48'),
(27, 'Eaves At Lawrenceville', '6331 Town Court North, Lawrence Township, NJ, USA', '40.2852839', '-74.6772344', '100', '40.2852839', '-74.6772344', '55', '22', '3', 56, '', 1, 3, '2019-03-08 19:32:00'),
(28, 'Washington Square Park', 'Washington Square Park, New York, NY, USA', '40.7308228', '-73.997332', '100', '40.7308228', '-73.997332', '66', '66', '3', 4, '', 1, 3, '2019-03-08 19:33:33'),
(29, 'Times Square Manhattan', 'Times Square, Manhattan, NY, USA', '40.759011', '-73.9844722', '100', '40.759011', '-73.9844722', '58758', '852', '3', 4, 'test desc.', 0, 3, '2019-03-08 19:34:41'),
(30, 'Mohave Room', 'Mohave County, AZ, USA', '35.2143346', '-113.7632828', '', '35.2143346', '-113.7632828', '536', '555', '3', 27, '', 0, 3, '2019-03-09 12:13:09'),
(31, 'Ontario Room', 'Ontario P0L, Canada', '51.4332367', '-82.3384062', '', '51.4332367', '-82.3384062', '2255', '2255', '3', 28, '', 0, 3, '2019-03-09 13:09:06'),
(32, 'Valley Room', 'Valley, WA 99181, USA', '48.1751444', '-117.7248441', '', '48.1751444', '-117.7248441', '454', '4454', '3', 27, '', 0, 3, '2019-03-09 13:14:32'),
(33, '47th Room', '47th St NE, Drake, ND 58736, USA', '48.094826', '-100.474329', '', '48.094826', '-100.474329', '55', '55', '3', 27, '', 0, 3, '2019-03-09 13:15:58'),
(34, 'SWEETWATR ', 'SWEETWATR STA, WY 82520, USA', '42.5421817', '-108.183704', '', '42.5421817', '-108.183704', '33', '33', '3', 38, '', 0, 3, '2019-03-09 14:14:04'),
(35, 'Test', 'Kothrud, Pune, Maharashtra, India', '18.5073985', '73.8076504', '', '18.5073985', '73.8076504', '1300', '1300', '7', 24, '', 0, 3, '2019-03-19 18:10:02'),
(36, 'Testing', 'Surat, Gujarat, India', '21.1702401', '72.8310607', '', '21.1702401', '72.8310607', '200', '200', '7', 24, '', 0, 3, '2019-03-19 18:31:39'),
(37, 'Tets', 'Gujarat, India', '22.258652', '71.1923805', '', '22.258652', '71.1923805', '100', '100', '7', 24, '', 0, 3, '2019-03-19 18:32:07'),
(38, 'Blue9', 'Bavdhan, Pune, Maharashtra, India', '18.5156035', '73.7819051', '', '18.5156035', '73.7819051', '1200', '1200', '7', 24, '', 0, 3, '2019-03-21 12:45:01'),
(39, 'Room11', 'Pune Municipal Corporation, Shivajinagar, Pune, Maharashtra, India', '18.5233237', '73.8534957', '', '18.5233237', '73.8534957', '200', '220', '7', 86, '', 0, 3, '2019-03-21 17:44:16'),
(40, 'Room2', 'Swargate Flyover, Pune, Maharashtra, India', '18.4979276', '73.8582215', '', '18.4979276', '73.8582215', '150', '250', '7', 86, '', 0, 3, '2019-03-21 17:48:57'),
(41, 'Room3', 'Swargate Flyover, Pune, Maharashtra, India', '18.4979276', '73.8582215', '', '18.4979276', '73.8582215', '170', '220', '7', 86, '', 0, 3, '2019-03-22 10:27:15'),
(42, 'Test Room 22', 'Pune, Maharashtra, India', '18.5204303', '73.8567437', '', '18.5204303', '73.8567437', '200', '200', '3', 41, '', 0, 3, '2019-04-05 17:43:00'),
(43, 'Test 1212', 'Unnamed Road, Lottie, LA 70756, USA', '30.5563027', '-91.6417799', '', '30.5563027', '-91.6417799', '1212', '1212', '3', 40, '', 0, 3, '2019-04-05 18:39:40'),
(44, 'Tt 2233', 'Forest Rd 49, Reserve, NM 87830, USA', '33.857711', '-108.7409582', '', '33.857711', '-108.7409582', '2233', '2233', '3', 27, '', 0, 3, '2019-04-05 18:50:19'),
(45, 'Test Vedio Room', 'Unnamed Road, Zacatecas, Mexico', '22.5148817', '-102.265422', '', '22.5148817', '-102.265422', '200', '200', '3', 77, 'test desc.', 0, 3, '2019-04-13 12:53:47'),
(46, 'Test', 'Datil, NM 87821, USA', '34.1449543', '-107.8429625', '', '34.1449543', '-107.8429625', '236', '236', '3', 39, 'desc', 0, 3, '2019-04-13 12:56:55'),
(47, 'Ddd', 'Forest Service Rd 136, Happy Jack, AZ 86024, USA', '34.6344196', '-111.3214216', '', '34.6344196', '-111.3214216', '22', '22', '3', 77, 'desc.', 0, 3, '2019-04-13 13:55:07'),
(48, 'Testtt', 'Co Rd 480, Coleman, TX 76834, USA', '32.0171402', '-99.5661845', '', '32.0171402', '-99.5661845', '1212', '1212', '3', 28, 'dd', 0, 3, '2019-04-13 13:58:12'),
(49, 'Test Room Vedio Img', '662 County Rd 2722, Lometa, TX 76853, USA', '31.2171118', '-98.3933692', '', '31.2171118', '-98.3933692', '200', '300', '3', 28, 'test desc', 0, 3, '2019-04-13 14:02:40'),
(50, 'Commerzone IT Park', 'Commerzone IT Park, Yerawada, Pune, Maharashtra, India', '18.5602898', '73.883805', '', '18.5602898', '73.883805', '200', '200', '7', 86, 'This room is for IT professionals to exchange Ideas', 0, 3, '2019-05-08 14:52:51'),
(51, 'Sandy\'s Apartment', 'Nisarg Pooja, Wakad Road, Kaspate Wasti, Wakad, Pimpri-Chinchwad, Maharashtra, India', '18.5906725', '73.773848', '', '18.5906725', '73.773848', '200', '300', '7', 86, 'This is a beautiful apartment complex in Wakad, Pune', 0, 3, '2019-05-08 14:58:04'),
(52, 'Mercer Mall Room', 'Mercer Mall, Brunswick Pike, Lawrence Township, NJ, USA', '40.2963067', '-74.6816945', '', '40.2963067', '-74.6816945', '100', '100', '3', 56, 'A strip mall with grocery, coffee shop and a liquor store', 0, 3, '2019-06-10 23:18:05'),
(53, 'Bonefish Grill At Mercer Mall', 'Bonefish Grill, Brunswick Pike, Lawrence Township, NJ, USA', '40.2974965', '-74.6831878', '', '40.2974965', '-74.6831878', '50', '50', '3', 56, 'Contemporary grill chain offering a seafood-centric menu, plus steaks & cocktails.', 1, 3, '2019-06-10 23:23:07'),
(54, 'Bonefish Grill At Mercer Mall', 'Bonefish Grill, Brunswick Pike, Lawrence Township, NJ, USA', '40.2974965', '-74.6831878', '', '40.2974965', '-74.6831878', '50', '50', '3', 56, 'Contemporary grill chain offering a seafood-centric menu, plus steaks & cocktails.', 0, 3, '2019-06-10 23:34:39'),
(55, 'Startbucks At Lawrence Townshi', 'Verizon, Lawrence Township, Lawrenceville, NJ, USA', '40.2976572', '-74.6816334', '', '40.2976572', '-74.6816334', '50', '50', '3', 56, 'Seattle-based coffeehouse chain known for its signature roasts, light bites and WiFi availability.', 0, 3, '2019-06-10 23:44:39'),
(56, 'Sakars Apartment Complex', '11258 Point Sylvan Circle, Orlando, FL, USA', '28.5610788', '-81.2214195', '', '28.5610788', '-81.2214195', '50', '50', '3', 35, 'Our dearest Sakar lives around here', 0, 3, '2019-06-11 07:00:53'),
(57, 'Sarthak Apartment Complex', '6 Town Court North, Lawrence Township, NJ, USA', '40.2861212', '-74.6785214', '', '40.2861212', '-74.6785214', '50', '50', '3', 56, '', 0, 3, '2019-06-11 07:01:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_image`
--

CREATE TABLE `tbl_room_image` (
  `room_image_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_image_path` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_room_image`
--

INSERT INTO `tbl_room_image` (`room_image_id`, `room_id`, `room_image_path`, `created_by`, `created_date`) VALUES
(9, 42, 'uploads/room_images/Desert1_@1.jpg', 3, '2019-04-05 17:43:00'),
(21, 43, 'uploads/room_images/Desert1_@2.jpg', 3, '2019-04-05 18:39:40'),
(8, 42, 'uploads/room_images/Chrysanthemum!@.jpg', 3, '2019-04-05 17:43:00'),
(17, 29, 'uploads/room_images/Desert1_@.jpg', 3, '2019-04-05 18:11:54'),
(10, 42, 'uploads/room_images/Hydrangeas.jpg', 3, '2019-04-05 17:43:00'),
(11, 42, 'uploads/room_images/Jellyfish_!@.jpg', 3, '2019-04-05 17:43:00'),
(12, 42, 'uploads/room_images/Lighthouse.jpg', 3, '2019-04-05 17:43:01'),
(15, 29, 'uploads/room_images/Penguins.jpg', 3, '2019-04-05 18:04:11'),
(20, 43, 'uploads/room_images/Chrysanthemum!@1.jpg', 3, '2019-04-05 18:39:40'),
(19, 29, 'uploads/room_images/Jellyfish_!@1.jpg', 3, '2019-04-05 18:11:54'),
(25, 44, 'uploads/room_images/Desert1_@3.jpg', 3, '2019-04-05 18:50:19'),
(23, 43, 'uploads/room_images/Jellyfish_!@2.jpg', 3, '2019-04-05 18:39:41'),
(24, 43, 'uploads/room_images/Koala.jpg', 3, '2019-04-05 18:39:41'),
(26, 44, 'uploads/room_images/Hydrangeas1.jpg', 3, '2019-04-05 18:50:19'),
(30, 45, 'uploads/room_images/home_developer2_offer_gallery1_-_Copy_-_Copy.jpg', 3, '2019-04-13 12:53:47'),
(28, 44, 'uploads/room_images/Koala1.jpg', 3, '2019-04-05 18:50:19'),
(29, 44, 'uploads/room_images/Penguins1.jpg', 3, '2019-04-05 18:50:19'),
(31, 45, 'uploads/room_images/home_developer2_offer_gallery3.jpg', 3, '2019-04-13 12:53:47'),
(32, 45, 'uploads/room_images/home_developer2_offer2_-_Copy.jpg', 3, '2019-04-13 12:53:47'),
(33, 46, 'uploads/room_images/Desert1_@4.jpg', 3, '2019-04-13 12:56:55'),
(34, 46, 'uploads/room_images/Penguins2.jpg', 3, '2019-04-13 12:56:55'),
(35, 46, 'uploads/room_images/Tulips.jpg', 3, '2019-04-13 12:56:55'),
(36, 48, 'uploads/room_images/Hydrangeas2.jpg', 3, '2019-04-13 13:58:22'),
(42, 48, 'uploads/room_images/Chrysanthemum!@3.jpg', 3, '2019-04-13 18:03:29'),
(38, 49, 'uploads/room_images/Chrysanthemum!@2.jpg', 3, '2019-04-13 14:03:00'),
(39, 49, 'uploads/room_images/Desert1_@5.jpg', 3, '2019-04-13 14:03:01'),
(40, 49, 'uploads/room_images/Hydrangeas3.jpg', 3, '2019-04-13 14:03:01'),
(41, 49, 'uploads/room_images/Lighthouse1.jpg', 3, '2019-04-13 14:03:01'),
(43, 48, 'uploads/room_images/Desert1_@6.jpg', 3, '2019-04-13 18:03:29'),
(44, 48, 'uploads/room_images/Jellyfish_!@3.jpg', 3, '2019-04-13 18:03:30'),
(45, 48, 'uploads/room_images/Tulips1.jpg', 3, '2019-04-13 18:03:30'),
(46, 29, 'uploads/room_images/kitchen_panorama_virtual_tour_-_1280x640.jpg', 3, '2019-04-15 14:27:11'),
(47, 29, 'uploads/room_images/360_panorama_home_interior_-_6284x3142.jpg', 3, '2019-04-15 14:27:12'),
(48, 50, 'uploads/room_images/CZ1.jpg', 3, '2019-05-08 14:52:51'),
(49, 50, 'uploads/room_images/CZ2.jpg', 3, '2019-05-08 14:52:51'),
(50, 50, 'uploads/room_images/CZ3.jpg', 3, '2019-05-08 14:52:51'),
(51, 51, 'uploads/room_images/NP1.jpg', 3, '2019-05-08 14:58:04'),
(52, 51, 'uploads/room_images/NP2.jpg', 3, '2019-05-08 14:58:04'),
(53, 51, 'uploads/room_images/NP3.jpg', 3, '2019-05-08 14:58:04'),
(54, 51, 'uploads/room_images/NP4.jpg', 3, '2019-05-08 14:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_vedio`
--

CREATE TABLE `tbl_room_vedio` (
  `room_vedio_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_vedio_path` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_room_vedio`
--

INSERT INTO `tbl_room_vedio` (`room_vedio_id`, `room_id`, `room_vedio_path`, `created_by`, `created_date`) VALUES
(7, 48, 'uploads/room_vedio/Brett_Ledbetter-_Person_over_Player_-_Copy1.mp4', 3, '2019-04-13 18:03:29'),
(2, 49, 'uploads/room_vedio/Brett_Ledbetter-_Person_over_Player_-_Copy_(2)1.mp4', 3, '2019-04-13 14:02:50'),
(3, 49, 'uploads/room_vedio/Brett_Ledbetter-_Person_over_Player_-_Copy.mp4', 3, '2019-04-13 14:03:00'),
(6, 48, 'uploads/room_vedio/Brett_Ledbetter-_Person_over_Player_-_Copy_(2).mp4', 3, '2019-04-13 18:03:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_state`
--

CREATE TABLE `tbl_state` (
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `is_delete` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_state`
--

INSERT INTO `tbl_state` (`state_id`, `country_id`, `state_name`, `is_delete`, `created_by`, `created_date`) VALUES
(1, 1, 'Maharashtra', 1, 3, '2019-03-04 16:07:54'),
(2, 1, 'Delhi', 1, 3, '2019-03-04 16:08:17'),
(3, 1, 'Goa', 1, 3, '2019-03-04 16:08:28'),
(4, 3, 'New York - NY', 0, 3, '2019-03-06 12:56:31'),
(5, 3, 'Sacremanto', 1, 3, '2019-03-06 12:59:40'),
(6, 1, 'Uttar Predesh', 1, 3, '2019-03-06 12:59:43'),
(7, 1, 'Hyderabad', 1, 3, '2019-03-06 13:00:52'),
(8, 1, 'Sacremanto', 1, 3, '2019-03-06 13:01:25'),
(9, 3, 'Sacremanto', 1, 3, '2019-03-06 13:30:09'),
(10, 7, 'Sacremanto', 1, 3, '2019-03-06 13:45:50'),
(11, 3, 'Sacremanto', 1, 3, '2019-03-06 13:47:20'),
(12, 3, 'Sacremanto', 1, 3, '2019-03-06 14:48:58'),
(13, 3, 'Sacremanto', 1, 3, '2019-03-06 14:54:34'),
(14, 7, 'Madhya Pradesh', 1, 3, '2019-03-06 14:58:39'),
(15, 3, 'Madhya Pradesh', 1, 3, '2019-03-06 15:19:11'),
(16, 3, 'Madhya Pradesh', 1, 3, '2019-03-06 15:23:35'),
(17, 7, 'Uttar Pradesh', 1, 3, '2019-03-06 15:30:21'),
(18, 7, 'Uttar Pradesh', 1, 3, '2019-03-06 15:30:40'),
(19, 7, 'sacremanto', 1, 3, '2019-03-06 15:45:49'),
(20, 3, 'Sacremanto', 1, 3, '2019-03-06 17:27:50'),
(21, 7, 'Sacremanto', 1, 3, '2019-03-06 17:28:03'),
(22, 7, 'Madhya Pradesh', 1, 3, '2019-03-06 17:53:21'),
(23, 7, 'Sacremanto', 1, 3, '2019-03-06 18:06:12'),
(24, 7, 'Uttar Pradesh', 0, 3, '2019-03-06 18:48:45'),
(25, 7, 'Madhya Pradesh', 0, 3, '2019-03-07 11:50:01'),
(26, 7, 'avc', 1, 3, '2019-03-07 11:50:07'),
(27, 3, 'Alabama - AL', 0, 3, '2019-03-08 12:31:24'),
(28, 3, 'Alaska - AK', 0, 3, '2019-03-08 12:31:34'),
(29, 3, 'Arizona - AZ', 0, 3, '2019-03-08 12:32:51'),
(30, 3, 'Arkansas - AR', 0, 3, '2019-03-08 12:33:00'),
(31, 3, 'California - CA', 0, 3, '2019-03-08 12:33:02'),
(32, 3, 'Colorado - CO', 0, 3, '2019-03-08 12:33:07'),
(33, 3, 'Connecticut - CT', 0, 3, '2019-03-08 12:33:15'),
(34, 3, 'Delaware - DE', 0, 3, '2019-03-08 12:33:21'),
(35, 3, 'Florida - FL', 0, 3, '2019-03-08 12:33:26'),
(36, 3, 'Georgia - GA', 0, 3, '2019-03-08 12:33:31'),
(37, 3, 'Hawaii - HI', 0, 3, '2019-03-08 12:33:43'),
(38, 3, 'Idaho - ID', 0, 3, '2019-03-08 12:33:51'),
(39, 3, 'Illinois - IL', 0, 3, '2019-03-08 12:33:58'),
(40, 3, 'Indiana - IN', 0, 3, '2019-03-08 12:34:03'),
(41, 3, 'Iowa - IA', 0, 3, '2019-03-08 12:34:13'),
(42, 3, 'Kansas - KS', 0, 3, '2019-03-08 12:34:20'),
(43, 3, 'Kentucky - KY', 0, 3, '2019-03-08 12:34:26'),
(44, 3, 'Louisiana - LA', 0, 3, '2019-03-08 12:34:34'),
(45, 3, 'Maine - ME', 0, 3, '2019-03-08 12:34:40'),
(46, 3, 'Maryland - MD', 0, 3, '2019-03-08 12:34:45'),
(47, 3, 'Massachusetts - MA', 0, 3, '2019-03-08 12:34:50'),
(48, 3, 'Michigan - MI', 0, 3, '2019-03-08 12:34:59'),
(49, 3, 'Minnesota - MN', 0, 3, '2019-03-08 12:35:06'),
(50, 3, 'Mississippi - MS', 0, 3, '2019-03-08 12:35:19'),
(51, 3, 'Missouri - MO', 0, 3, '2019-03-08 12:35:24'),
(52, 3, 'Montana - MT', 0, 3, '2019-03-08 12:35:29'),
(53, 3, 'Nebraska - NE', 0, 3, '2019-03-08 12:35:34'),
(54, 3, 'Nevada - NV', 0, 3, '2019-03-08 12:35:41'),
(55, 3, 'New Hampshire - NH', 0, 3, '2019-03-08 12:35:46'),
(56, 3, 'New Jersey - NJ', 0, 3, '2019-03-08 12:35:51'),
(57, 3, 'New Mexico - NM', 0, 3, '2019-03-08 12:35:56'),
(58, 3, 'North Carolina - NC', 0, 3, '2019-03-08 12:36:13'),
(59, 3, 'North Dakota - ND', 0, 3, '2019-03-08 12:36:19'),
(60, 3, 'Ohio - OH', 0, 3, '2019-03-08 12:36:25'),
(61, 3, 'Oklahoma - OK', 0, 3, '2019-03-08 12:36:31'),
(62, 3, 'Oregon - OR', 0, 3, '2019-03-08 12:36:37'),
(63, 3, 'Pennsylvania - PA', 0, 3, '2019-03-08 12:36:44'),
(64, 3, 'Rhode Island - RI', 0, 3, '2019-03-08 12:36:49'),
(65, 3, 'South Carolina - SC', 0, 3, '2019-03-08 12:36:54'),
(66, 3, 'South Dakota - SD', 0, 3, '2019-03-08 12:36:59'),
(67, 3, 'Tennessee - TN', 0, 3, '2019-03-08 12:37:04'),
(68, 3, 'Texas - TX', 0, 3, '2019-03-08 12:37:09'),
(69, 3, 'Utah - UT', 0, 3, '2019-03-08 12:37:14'),
(70, 3, 'Vermont - VT', 0, 3, '2019-03-08 12:37:18'),
(71, 3, 'Virginia - VA', 0, 3, '2019-03-08 12:37:24'),
(72, 3, 'Washington - WA', 0, 3, '2019-03-08 12:37:29'),
(73, 3, 'West Virginia - WV', 0, 3, '2019-03-08 12:37:34'),
(74, 3, 'Wisconsin - WI', 0, 3, '2019-03-08 12:37:39'),
(75, 3, 'Wyoming - WY', 0, 3, '2019-03-08 12:37:44'),
(76, 3, 'US Commonwealth and Territories', 0, 3, '2019-03-08 12:38:02'),
(77, 3, 'American Samoa - AS', 0, 3, '2019-03-08 12:38:09'),
(78, 3, 'District of Columbia - DC', 0, 3, '2019-03-08 12:38:47'),
(79, 3, 'Federated States of Micronesia - FM', 0, 3, '2019-03-08 12:38:54'),
(80, 3, 'Guam - GU', 0, 3, '2019-03-08 12:39:01'),
(81, 3, 'Marshall Islands - MH', 0, 3, '2019-03-08 12:39:07'),
(82, 3, 'Northern Mariana Islands - MP', 0, 3, '2019-03-08 12:39:14'),
(83, 3, 'Palau - PW', 0, 3, '2019-03-08 12:39:21'),
(84, 3, 'Puerto Rico - PR', 0, 3, '2019-03-08 12:39:26'),
(85, 3, 'Virgin Islands - VI', 0, 3, '2019-03-08 12:39:35'),
(86, 7, 'Maharashtra', 0, 3, '2019-03-21 17:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `d_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` longtext COLLATE utf8_unicode_ci NOT NULL,
  `f_g_image_path` longtext COLLATE utf8_unicode_ci NOT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_google_user` int(11) NOT NULL,
  `is_fb_user` int(11) NOT NULL,
  `token` longtext COLLATE utf8_unicode_ci NOT NULL,
  `otp` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `is_verified` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `password`, `d_password`, `display_name`, `profile_picture`, `f_g_image_path`, `uid`, `is_google_user`, `is_fb_user`, `token`, `otp`, `is_verified`, `is_active`, `is_delete`, `created_date`) VALUES
(1, 'satisha@indiawebinfotech.com', '', '', 'Satish Adhalrao', '', 'https://lh4.googleusercontent.com/-0t4HOL8PXrc/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rc__bnn_AT4dOK8Ox1-qQgF3X9IrA/s96-c/photo.jpg', 'Sg1GW20JyIeqUchy0LtM0295pVU2', 1, 0, '', '', '', 0, 0, '2019-04-05 16:10:00'),
(2, 'satish.adhalrao02@gmail.com', '', '', 'Satish Adhalrao', '', 'https://lh4.googleusercontent.com/-M9M4p8ifA9Q/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rcjdf-5pzdAxOZsdBAiQJ9YwHxVhw/s96-c/photo.jpg', 'mEIoqmF6fzMHYBroox05BLpaMcr1', 0, 1, '', '', '', 0, 0, '2019-04-05 16:10:23'),
(3, 'Satish@indiawebinfotech.com', '9dbb80f38ddbf853ea552e67f18b9b4c', 'Satish123', 'Satish', 'uploads/user_reg_image/file34.png', '', '', 0, 0, '488182b9209a6a487e1cd3be89fe907b', '3850', '1', 0, 0, '2019-04-05 18:02:08'),
(4, 'jain1408sandy@gmail.com', '25d55ad283aa400af464c76d713c07ad', '12345678', 'test', 'uploads/user_reg_image/file35.png', '', '', 0, 0, 'fc7cb9dec0172257d6216f1b87e6f9b8', '5043', '1', 0, 0, '2019-04-09 16:23:54'),
(5, 'dgg@fgg.tttt', '25d55ad283aa400af464c76d713c07ad', '12345678', 'ertfg', 'uploads/user_reg_image/file36.png', '', '', 0, 0, 'c44a3a5223763004e8d098442ed4d742', '4562', '', 0, 0, '2019-04-09 17:48:34'),
(6, 'vikas@gmail.com', '1bab029f0250f2ac1c86db5f824b3442', 'Vikas123', 'Vikas', 'uploads/user_reg_image/file37.png', '', '', 0, 0, '1f0e0feab122343065a0e9b8cb95cea1', '3045', '1', 0, 0, '2019-04-10 15:20:33'),
(7, 'bala@gmail.com', '4b89fb19f2e31e047b376d2aea74989d', 'Bala112', 'Bala', '', '', '', 0, 0, 'd003ce44b3189a450edd1c12b81d1dea', '9562', '', 0, 0, '2019-04-10 18:14:20'),
(8, 'tushar@gmail.com', '2fd7f5256055b95b54493febcdc77060', 'Tushar123', 'Tushar', '', '', '', 0, 0, '09b4616381677c02d02f891e4f69d2df', '2805', '1', 0, 0, '2019-04-10 18:16:27'),
(9, 'shree@gmail.com', '951eb7ebcf0460e42f2c259ee06cbf5e', 'Shree122', 'Shree', '', '', '', 0, 0, '431bffa87b32f97e06e32b23a5a95749', '8130', '', 0, 0, '2019-04-10 18:24:37'),
(10, 'ashishp@indiawebinfotech.com', 'a2d03d878a0c6475e7a5678d68802196', 'Ashish122', 'sham', 'uploads/user_reg_image/file38.png', '', '', 0, 0, 'f8a497260580c5e452af0a76773193d9', '0873', '1', 0, 0, '2019-04-10 18:28:16'),
(11, 'rim@gmail.com', 'dcbdf48e5826b4eaf2961e086f86baf8', 'Rim1124', 'rim', 'uploads/user_reg_image/file39.png', '', '', 0, 0, '1beaae0f82076e3c1be4a4e1457918f6', '9826', '', 0, 0, '2019-04-10 18:30:43'),
(12, 'test@gmail.com', 'f925916e2754e5e03f75dd58a5733251', 'Test@123', 'TestUser', 'uploads/user_reg_image/file40.png', '', '', 0, 0, '8dc1bd3a74a9d6ebe4875db996bf6ff1', '3841', '', 0, 0, '2019-04-10 18:56:00'),
(13, 'bhargav@mailinator.com', 'f925916e2754e5e03f75dd58a5733251', 'Test@123', 'TestUser', 'uploads/user_reg_image/file41.png', '', '', 0, 0, '431f4e8aa549e703572a1661aba1bba6', '8629', '1', 0, 0, '2019-04-10 18:58:34'),
(14, 'nitish@innolytic.co', 'ac7e5b93f648810c72e0172bc170d65b', 'Nitish123', 'Nitish', 'uploads/user_reg_image/file42.png', '', '', 0, 0, '55d78dd576c0d4b16db6b20cb43d93b5', '7619', '1', 0, 0, '2019-04-10 19:02:13'),
(15, 'raj1@gmail.com', '6491210490b15fd925058e8dd9ff3d5d', '123Raj', 'raj', 'uploads/user_reg_image/file43.png', '', '', 0, 0, '8906cfb489e0ec50e74746b64c7d2fd7', '4395', '', 0, 0, '2019-04-10 22:03:13'),
(16, 'amita@indiawebinfotech.com', 'e6339abf83d123e9189595453535a941', 'Amit1234', 'Amit', 'uploads/user_reg_image/file44.png', '', '', 0, 0, 'fb31bc38a8a8e19f445827133307b488', '0972', '1', 0, 0, '2019-04-11 11:51:40'),
(17, 'patil@gmail.com', 'adbd75ad09b4947588d76e063aec8dad', 'Patil1234', 'Patil', 'uploads/user_reg_image/file45.png', '', '', 0, 0, '1bd5ecd9dd710de34f62c8fcd103d8d6', '7425', '1', 0, 0, '2019-04-11 15:46:18'),
(18, 'test@test.com', '25d55ad283aa400af464c76d713c07ad', '12345678', 'test@test.com', 'uploads/user_reg_image/file46.png', '', '', 0, 0, 'cdbef63da7440312738123bb10b6ab68', '9215', '', 0, 0, '2019-04-12 16:28:20'),
(19, 'test@123.com', '25d55ad283aa400af464c76d713c07ad', '12345678', 'test', 'uploads/user_reg_image/file47.png', '', '', 0, 0, 'e7d6380b03501612f955a2eed0cbebe4', '1526', '', 0, 0, '2019-04-12 16:29:36'),
(20, 'sff@sd.vvo', '4767a7a9b57c7f9726ea0979960c866d', '124456788', 'Sandeep', 'uploads/user_reg_image/file48.png', '', '', 0, 0, 'f8df47a6b2be511305c8ab1a2c1b5644', '0348', '', 0, 0, '2019-04-14 17:22:58'),
(21, 'sfdg@dff.vvo', 'fae8a943573d12bb5a79917c1e67ba81', '123456u77', 'affdgd', 'uploads/user_reg_image/file49.png', '', '', 0, 0, '6231da0c90255e44ded104407677550d', '7482', '', 0, 0, '2019-04-14 17:23:37'),
(22, 'dg@gh.gh', '8df7fce76a106263acca4fe318a02fb2', '1344556667', 'bbnbh', 'uploads/user_reg_image/file50.png', '', '', 0, 0, '042fb4438b6b3af630d320339622e11d', '2395', '', 0, 0, '2019-04-14 17:24:37'),
(23, 'sdff@sfff.com', '25d55ad283aa400af464c76d713c07ad', '12345678', 'diff', 'uploads/user_reg_image/file51.png', '', '', 0, 0, '0cdb6be94559dab7854f5fbec4968553', '1736', '', 0, 0, '2019-04-14 17:30:41'),
(24, 'sfdvg@fgg.vvbb', '25f9e794323b453885f5181f1b624d0b', '123456789', 'sanded did', 'uploads/user_reg_image/file52.png', '', '', 0, 0, 'e7dcb2a81ff9fd9aa4bc08918ac8e724', '3218', '', 0, 0, '2019-04-14 17:34:23'),
(25, 'dfgf@fgfhhf.sfgg', 'f78f25706d92aa2d4e3b078c68fc60eb', '12334566788', 'dfffsggf', 'uploads/user_reg_image/file53.png', '', '', 0, 0, 'bb2c509ef26cc1d2924fecd2015300cd', '8346', '', 0, 0, '2019-04-14 17:35:10'),
(26, 'dgdd@fggg.dfg', '9d35c201321ab47e91a43157651eda3a', '1234456667', 'saddest', 'uploads/user_reg_image/file54.png', '', '', 0, 0, '9b87cb724ece2bc3023115937e869dbd', '9621', '', 0, 0, '2019-04-14 17:36:46'),
(27, 'test@test1.com', 'd66aab3154ac90dcbc37c9957464a365', '1234567789', 'test', 'uploads/user_reg_image/file55.png', '', '', 0, 0, '0038b7bc51ed82c98dfe4b2beca4303a', '4570', '', 0, 0, '2019-04-14 18:41:25'),
(28, 'asd@test.com', '25d55ad283aa400af464c76d713c07ad', '12345678', 'sabdne', 'uploads/user_reg_image/file56.png', '', '', 0, 0, 'af33fa6ad906870f8063538399dde687', '7092', '', 0, 0, '2019-04-18 14:39:47'),
(29, 'satishk@indiawebinfotech.com', '8376a2a7a205368110bf946a4595b3cf', 'Satish1234', 'Satish', 'uploads/user_reg_image/file57.png', '', '', 0, 0, '3dde5f2735338c7f2c8d3bb85d11ca10', '2356', '1', 0, 0, '2019-04-18 15:48:35'),
(30, 'prafullp@indiawebinfotech.com', '53b0a239e8f8772c15b9509652360777', 'Prafull123', 'Prafull', 'uploads/user_reg_image/file58.png', '', '', 0, 0, 'bfc8bc1b9fbb712636d7a1e43841769c', '2085', '', 0, 0, '2019-04-22 17:14:04'),
(31, 'parish@gmail.com', '2aa5ecd344e6a2efec01245eaa4a8f42', 'Parish123', 'parish', 'uploads/user_reg_image/file59.png', '', '', 0, 0, '54d5b25c4a41003a23eab1c9d5262d89', '6512', '', 0, 0, '2019-04-22 17:18:37'),
(32, 'Test7@gmail.com', '40bca5be7d6a2a5f747fb102e4082f6e', 'Teat1234', 'Test', 'uploads/user_reg_image/file60.png', '', '', 0, 0, 'bedde638542fdb45dfab80a8f9814802', '5863', '', 0, 0, '2019-04-22 17:27:01'),
(33, 'test8@gmail.com', '2c9341ca4cf3d87b9e4eb905d6a3ec45', 'Test1234', 'test', 'uploads/user_reg_image/file61.png', '', '', 0, 0, 'ee5f34174a45e07090ce4d61404b2e81', '5342', '1', 0, 0, '2019-04-22 17:39:21'),
(34, 'fggfg@fgg.vv', '5c595b48334cede936ab1f11e595a201', 'Addf@4456', 'dfsdsfdg', 'uploads/user_reg_image/file62.png', '', '', 0, 0, 'af618da6ea53939af2e9ac1f8691131f', '8047', '', 0, 0, '2019-05-15 18:14:15'),
(35, 'dfgg@fgg.vbb', '531fc98d33f7640cd7e8996d4835f2d4', '1223WEDdA', 'XX got', 'uploads/user_reg_image/file63.png', '', '', 0, 0, '10da14d82270a4bb2a814b2b869492b6', '6473', '', 0, 0, '2019-05-15 18:17:43'),
(36, 'sddsfg@fgg.hh', '925e40401876285984b2d953e04d0747', 'szA234@t5', 'dggdssg', 'uploads/user_reg_image/file64.png', '', '', 0, 0, '66a563d68a8689279fa14f6d736bd705', '7640', '', 0, 0, '2019-05-15 18:19:43'),
(37, 'sarthak@knockr.us', '45250fe7816a29973f078c1d29b70e12', 'Knockr2018', 'Sarthak  Sawarkar', 'uploads/user_reg_image/file65.png', '', '', 0, 0, '0294931206776039e07531d088cf1771', '6342', '', 0, 0, '2019-06-10 18:07:11'),
(38, 'sarthaksavarkar@gmail.com', '45250fe7816a29973f078c1d29b70e12', 'Knockr2018', 'Sarthak Sawarkar', 'uploads/user_reg_image/file66.png', '', '', 0, 0, 'e0442ed45f0fc4ac4f5d4e145622bf77', '1203', '1', 0, 0, '2019-06-10 18:16:53'),
(39, 'sandeep.jain@aptify.com', '110a6a26bb90190391c53e9545095d62', 'A@123456a', 'abc', 'uploads/user_reg_image/file67.png', '', '', 0, 0, '90a9c1d6e1706d4e40c88e2e75ce0e14', '6409', '1', 0, 0, '2019-06-10 18:17:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin_users`
--
ALTER TABLE `tbl_admin_users`
  ADD PRIMARY KEY (`admin_users_id`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `tbl_room`
--
ALTER TABLE `tbl_room`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `tbl_room_image`
--
ALTER TABLE `tbl_room_image`
  ADD PRIMARY KEY (`room_image_id`);

--
-- Indexes for table `tbl_room_vedio`
--
ALTER TABLE `tbl_room_vedio`
  ADD PRIMARY KEY (`room_vedio_id`);

--
-- Indexes for table `tbl_state`
--
ALTER TABLE `tbl_state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin_users`
--
ALTER TABLE `tbl_admin_users`
  MODIFY `admin_users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_room`
--
ALTER TABLE `tbl_room`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `tbl_room_image`
--
ALTER TABLE `tbl_room_image`
  MODIFY `room_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tbl_room_vedio`
--
ALTER TABLE `tbl_room_vedio`
  MODIFY `room_vedio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_state`
--
ALTER TABLE `tbl_state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
